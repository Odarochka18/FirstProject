from telebot import types
import telebot

from config import TOKEN

bot = telebot.TeleBot(TOKEN)
my_chat_id = 1316066396


@bot.message_handler(commands=['start'])
def start(message):
    keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    button_1 = types.KeyboardButton(text="Услуги")
    button_2 = types.KeyboardButton(text="О нас")
    button_3 = types.KeyboardButton(text="Оставить заявку")
    keyboard.add(button_1, button_2, button_3)
    bot.send_message(message.chat.id, 'Приветствуем! Мы компания. Добро пожаловать!', reply_markup=keyboard)


# Кнопки в сообщении


def info(message):
    keyboard = types.InlineKeyboardMarkup()
    url_button = types.InlineKeyboardButton(text='Перейти на мою страницу', url="https://vk.com/odarka_kruk")
    keyboard.add(url_button)
    bot.send_message(message.chat.id, "Привет! Нажми на кномку и перейди на мою страницу", reply_markup=keyboard)


def send_request(message):
    mes = f'Новая заявка: {message.text}'
    bot.send_message(my_chat_id, mes)
    bot.send_message(message.chat.id, "Спасибо за заявку! Наши специалисты скоро с вами свяжутся")


def send_service(message):
    bot.send_message(message.chat.id, "1. Составить годовой отчёт")
    bot.send_message(message.chat.id, "2. Оплатить налоги за ТО")
    bot.send_message(message.chat.id, "3. Расчитать бюджет")



@bot.message_handler(content_types=['text'])
def repeat_all_message(message):
    if message.text.lower() == 'о нас':
        info(message)
    if message.text.lower() == "оставить заявку":
        bot.send_message(message.chat.id, "Будем рады вас обслужить! Оставьте свои контактные данные.")
        bot.register_next_step_handler(message, send_request)
    if message.text.lower() == "услуги":
        send_service(message)


if __name__ == '__main__':
    bot.infinity_polling()
